﻿#include "MainWindow.hpp"
#include <QApplication>
#include <QtSql>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");

    db.setDatabaseName("faltometro.db");
    db.setHostName("localhost");

    if ( !db.open() )
    {
        qDebug() << db.lastError().text();
        return 0;
    }

    // Check database integrity
    QStringList tableNames = db.tables();
    QSqlQuery qry(db);

    qry.exec(QString("PRAGMA foreign_keys = ON"));

    qry.exec(QString(
                 "CREATE TABLE IF NOT EXISTS Holiday ( "
                 "Name TEXT, "
                 "Hdate DATE NOT NULL, "
                 "CONSTRAINT PK_Holiday PRIMARY KEY (Hdate)"
                 ")"));

    qry.exec(QString(
                 "CREATE TABLE IF NOT EXISTS Schedule ( "
                 "Day DECIMAL(1,0) NOT NULL CHECK(Day BETWEEN 0 AND 6), "
                 "Beginning TIME NOT NULL, "
                 "Ending TIME NOT NULL CHECK(Ending >= Beginning), "
                 "Subject INTEGER NOT NULL, "
                 "Local BLOB, "
                 "CONSTRAINT PK_Schedule "
                 "PRIMARY KEY (Day, Beginning), "
                 "CONSTRAINT FK_Day FOREIGN KEY (Day) "
                 "REFERENCES WeekdayNames (Day) ON DELETE CASCADE ON UPDATE CASCADE,"
                 "CONSTRAINT FK_Subject FOREIGN KEY (Subject) "
                 "REFERENCES Subject (ID) ON DELETE CASCADE ON UPDATE CASCADE"
                 ")"));

    qry.exec(QString(
                 "CREATE TABLE IF NOT EXISTS Subject ( "
                     "ID INTEGER NOT NULL, "
                     "Name TEXT NOT NULL, "
                     "Beginning DATE NOT NULL, "
                     "Ending DATE NOT NULL CHECK(Ending >= Beginning), "
                     "MinimumFrequency DECIMAL(5,2) NOT NULL CHECK(MinimumFrequency BETWEEN 0.0 AND 100.0), "
                     "CONSTRAINT PK_Subject PRIMARY KEY (ID), "
                     "CONSTRAINT UN_Name UNIQUE (Name)"
                 ")"));

    qry.exec(QString(
                 "CREATE TABLE IF NOT EXISTS Presence ( "
                     "ID INTEGER NOT NULL, "
                     "Pdate DATE, "
                     "Subject INTEGER NOT NULL, "
                     "CONSTRAINT PK_Presence "
                         "PRIMARY KEY (ID), "
                     "CONSTRAINT FK_Subject FOREIGN KEY (Subject) "
                         "REFERENCES Subject (ID) ON DELETE CASCADE ON UPDATE CASCADE"
                 ")"));

    if ( !tableNames.contains("WeekdayNames") )
    {
        qry.exec(QString("CREATE TABLE WeekdayNames ( "
                         "Day DECIMAL(1, 0) NOT NULL CHECK(Day BETWEEN 0 AND 6), "
                         "Name TEXT NOT NULL, "
                         "CONSTRAINT PK_Weekday PRIMARY KEY (Day), "
                         "CONSTRAINT UN_Name UNIQUE (Name) )"));
        qry.exec(QString("INSERT INTO WeekdayNames VALUES(0, '").append(QObject::tr("Domingo").append("')")));
        qry.exec(QString("INSERT INTO WeekdayNames VALUES(1, '").append(QObject::tr("Segunda").append("')")));
        qry.exec(QString("INSERT INTO WeekdayNames VALUES(2, '").append(QObject::tr("Terça").append("')")));
        qry.exec(QString("INSERT INTO WeekdayNames VALUES(3, '").append(QObject::tr("Quarta").append("')")));
        qry.exec(QString("INSERT INTO WeekdayNames VALUES(4, '").append(QObject::tr("Quinta").append("')")));
        qry.exec(QString("INSERT INTO WeekdayNames VALUES(5, '").append(QObject::tr("Sexta").append("')")));
        qry.exec(QString("INSERT INTO WeekdayNames VALUES(6, '").append(QObject::tr("Sábado").append("')")));
    }

    MainWindow w;
    w.show();

    int ret = a.exec();

    // Defrag database.
    qry.exec(QString("VACUUM"));

    db.close();

    return ret;
}
