#ifndef SUBJECTCALCULATIONS_HPP
#define SUBJECTCALCULATIONS_HPP

#include <QBitArray>
#include <QDate>
#include <QSqlDatabase>

namespace Subject
{
int getCompatibleWeekDay(int weekDay);

int getCurrentWeekDay();

int getClassesCount(const QDate& beginning, const QDate& end, const QBitArray& days, const QSqlDatabase &db);

double getCurrentFrequency(int classesCount, int misses);

int getHolidaysCount(const QDate& beginning, const QDate& end, const QBitArray& days, const QSqlDatabase &db);

int getRemainingMisses(int misses, int totalClasses, double minFrequency);
}
#endif // SUBJECTCALCULATIONS_HPP
