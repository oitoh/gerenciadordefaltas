#include "EditableHolidayModel.hpp"

EditableHolidayModel::EditableHolidayModel(QObject *parent, QSqlDatabase db)
    : QSqlQueryModel(parent), database(db)
{
    refresh();
}

Qt::ItemFlags EditableHolidayModel::flags(const QModelIndex &index) const
{
    Qt::ItemFlags flags = QSqlQueryModel::flags(index);
    if (index.column() >= 0 && index.column() <= 2)
        flags |= Qt::ItemIsEditable;
    return flags;
}

bool EditableHolidayModel::setData(const QModelIndex &index, const QVariant &value, int)
{
    if (index.column() < 0 || index.column() > 2)
        return false;

    bool ok;
    int row, column = index.column();
    QString name;
    QDate newValue, oldBegin, oldEnd;
    switch(column)
    {
    case 0:
        ok = setName(data(QSqlQueryModel::index(index.row(), 0)).toString(), value.toString());
        qDebug() << "oldname" << data(QSqlQueryModel::index(index.row(), 0)).toString() << " newname " << value.toString();
        break;
    case 1:
    case 2:
        row = index.row();
        name = data(QSqlQueryModel::index(row, 0)).toString();
        newValue = QDate::fromString(value.toString(), "d/M/yyyy");
        qDebug() << "Segue o biscoito: " << newValue;
        switch(column)
        {
        case 1:
            oldBegin = QDate::fromString(data(QSqlQueryModel::index(row, 1)).toString(), "d/M/yyyy");
            qDebug() << "Data incial: " << oldBegin;
            if ( newValue < oldBegin )
                ok = setEarlierBegin(name, oldBegin, newValue);
            else if ( newValue > oldBegin )
                ok = setLaterBegin(name, newValue);
            else
                ok = true;
            break;
        case 2:
            oldEnd = QDate::fromString(data(QSqlQueryModel::index(row, 2)).toString(), "d/M/yyyy");
            qDebug() << "Data final: " << oldEnd;
            if ( newValue < oldEnd )
                ok = setEarlierEnd(name, newValue);
            else if ( newValue > oldEnd )
                ok = setLaterEnd(name, oldEnd, newValue);
            else
                ok = true;
            break;
        }
        break;
    default:
        ok = false;
        break;
    }
    refresh();
    qDebug() << "Setup result: " << ok;
    return ok;
}

void EditableHolidayModel::refresh() {
    clear();
    setQuery("SELECT DISTINCT Name, strftime('%d/%m/%Y', MIN(Hdate)) AS Beginning, strftime('%d/%m/%Y', MAX(Hdate)) AS Ending FROM Holiday GROUP BY Name ORDER BY MIN(Hdate)", database);
    qDebug() << database.lastError();

    this->setHeaderData(0, Qt::Horizontal, QObject::tr("Nome"));
    this->setHeaderData(1, Qt::Horizontal, QObject::tr("Primeiro dia"));
    this->setHeaderData(2, Qt::Horizontal, QObject::tr("Último dia"));
}

bool EditableHolidayModel::setName(const QString &oldName, const QString &newName)
{
    QSqlQuery updateName(database);
    updateName.prepare("UPDATE Holiday SET Name = :newname WHERE Name = :oldname");
    updateName.bindValue(":newname", newName);
    updateName.bindValue(":oldname", oldName);
    bool ok = updateName.exec();
    if (!ok) updateName.lastError();
    return ok;
}

bool EditableHolidayModel::setEarlierBegin(const QString &name, const QDate &oldBegin, const QDate &newBegin)
{
    QSqlQuery updateBeginning(database);
    // Insert new dates
    updateBeginning.prepare("INSERT INTO Holiday (Name, Hdate) VALUES(:name, :date)");
    updateBeginning.bindValue(":name", name);

    QDate newDate = newBegin;
    bool ok = true;
    do
    {
        updateBeginning.bindValue(":date", newDate);
        newDate = newDate.addDays(1);
    } while( (ok = updateBeginning.exec()) && newDate < oldBegin );

    return ok;
}

bool EditableHolidayModel::setLaterBegin(const QString &name, const QDate &newBegin)
{
    bool ok;
    QSqlQuery updateBeginning(database);

    // Delete all of this holiday's date before the new beginning
    updateBeginning.prepare("DELETE FROM Holiday WHERE Name = :name AND Hdate <= :newbegin");
    updateBeginning.bindValue(":name", name);
    updateBeginning.bindValue(":newbegin", newBegin);
    ok = updateBeginning.exec();

    // Insert new beginning
    updateBeginning.prepare("INSERT INTO Holiday (Name, Hdate) VALUES(:name, :date)");
    updateBeginning.bindValue(":name", name);
    updateBeginning.bindValue(":date", newBegin);

    return ok && updateBeginning.exec();
}

bool EditableHolidayModel::setEarlierEnd(const QString &name, const QDate &newEnd)
{
    bool ok;
    QSqlQuery updateEnd(database);

    // Delete all of this holiday's date after the new end
    updateEnd.prepare("DELETE FROM Holiday WHERE Name = :name AND Hdate >= :newend");
    updateEnd.bindValue(":name", name);
    updateEnd.bindValue(":newend", newEnd);
    ok = updateEnd.exec();

    // Insert new end
    updateEnd.prepare("INSERT INTO Holiday (Name, Hdate) VALUES(:name, :date)");
    updateEnd.bindValue(":name", name);
    updateEnd.bindValue(":date", newEnd);

    return ok && updateEnd.exec();
}

bool EditableHolidayModel::setLaterEnd(const QString &name, const QDate &oldEnd, const QDate &newEnd)
{
    QSqlQuery updateEnd(database);
    // Insert new dates
    updateEnd.prepare("INSERT INTO Holiday (Name, Hdate) VALUES(:name, :date)");
    updateEnd.bindValue(":name", name);

    QDate newDate = oldEnd.addDays(1);
    bool ok = true;
    do
    {
        updateEnd.bindValue(":date", newDate.toString(Qt::ISODate));
        newDate = newDate.addDays(1);
    } while( (ok = updateEnd.exec()) && newDate <= newEnd );
    qDebug() << updateEnd.lastError();
    return ok;
}
