#ifndef EDITABLEHOLIDAYMODEL_HPP
#define EDITABLEHOLIDAYMODEL_HPP
#include <QtSql>

class EditableHolidayModel : public QSqlQueryModel {
public:
    EditableHolidayModel(QObject *parent, QSqlDatabase db);

    Qt::ItemFlags flags(const QModelIndex &index) const Q_DECL_OVERRIDE;

    bool setData(const QModelIndex &index, const QVariant &value,
                 int) Q_DECL_OVERRIDE;

    void refresh();

private:
    QSqlDatabase database;

    bool setName(const QString &oldName, const QString &newName);

    bool setEarlierBegin(const QString &name, const QDate &oldBegin,
                         const QDate &newBegin);

    bool setLaterBegin(const QString &name, const QDate &newBegin);

    bool setEarlierEnd(const QString &name, const QDate &newEnd);

    bool setLaterEnd(const QString &name, const QDate &oldEnd,
                     const QDate &newEnd);
};

#endif // EDITABLEHOLIDAYMODEL_HPP
