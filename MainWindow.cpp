#include "MainWindow.hpp"
#include "ui_MainWindow.h"
#include "EditableHolidayModel.hpp"
#include "EditableSubjectPresenceModel.hpp"
#include "ExtendedScheduleModel.hpp"
#include "subjectCalculations.hpp"

#include <QGeoCircle>
#include <QtSql>
#include <QtWidgets>

#define INVALID_WEEKDAY -1

/* TODO: Garantir que não marcará presença durante feriado, caso o usuário esteja a 20m de distancia do local, num feriado.
 *       Oferecer suporte a uma mesma aula de uma disciplina em um dia.
 */

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow), nextCheck(this) {
    ui->setupUi(this);

    QDate today = QDate::currentDate();
    int weekDay = Subject::getCompatibleWeekDay(today.dayOfWeek());

    // Initialize timer.
    nextCheck.setSingleShot(true);
    nextCheck.setTimerType(Qt::VeryCoarseTimer);

    this->blockSignals(true);

    // Model creation for the schedule and view association.
    ExtendedScheduleModel *scheduleModel =
            new ExtendedScheduleModel(0, QSqlDatabase::database());

    scheduleModel->setFilter(
                QString("Day = ").append(QString::number(weekDay)));
    QObject::connect(scheduleModel,
                     SIGNAL(dataChanged(QModelIndex, QModelIndex, QVector<int>)),
                     this, SLOT(updateNextClassAndSetTimer()));

    ui->scheduleWeekdayComboBox->blockSignals(true);
    ui->scheduleWeekdayComboBox->setCurrentIndex(weekDay);
    ui->scheduleWeekdayComboBox->blockSignals(false);

    // 0 - Sunday ...
    ui->scheduleTableView->setModel(scheduleModel);
    ui->scheduleTableView->setItemDelegate(
                new QSqlRelationalDelegate(ui->scheduleTableView));
    ui->scheduleTableView->hideColumn(0);
    ui->scheduleTableView->hideColumn(4);

    scheduleModel->select();

    // Model for the subject editor.
    EditableSubjectPresenceModel *queryEditableModel =
            new EditableSubjectPresenceModel(0, QSqlDatabase::database());
    ui->subjectTableView->setModel(queryEditableModel);
    ui->subjectTableView->hideColumn(0);

    // Map the subject combobox in the schedule and presence views.
    ui->scheduleSubjectComboBox->setModel(queryEditableModel);
    ui->scheduleSubjectComboBox->setModelColumn(1);
    ui->presenceSubjectComboBox->setModel(queryEditableModel);
    ui->presenceSubjectComboBox->setModelColumn(1);

    // Map presence.
    QSqlRelationalTableModel *relationalModel = new QSqlRelationalTableModel(0, QSqlDatabase::database());
    relationalModel->setTable("Presence");
    relationalModel->setHeaderData(0, Qt::Horizontal, "ID");
    relationalModel->setHeaderData(1, Qt::Horizontal, QObject::tr("Data"));
    relationalModel->setHeaderData(2, Qt::Horizontal, QObject::tr("Disciplina"));
    relationalModel->setRelation(2, QSqlRelation("Subject", "ID", "Name"));
    relationalModel->setSort(1, Qt::AscendingOrder);
    relationalModel->select();
    ui->presenceTableView->setModel(relationalModel);
    ui->presenceTableView->hideColumn(0);

    // Map subject schedule.
    scheduleModel = new ExtendedScheduleModel(0, QSqlDatabase::database());
    scheduleModel->setRelation(0, QSqlRelation("WeekdayNames", "Day", "Name"));
    ui->subjectScheduleTableView->setModel(scheduleModel);
    ui->subjectScheduleTableView->setItemDelegate(
                new QSqlRelationalDelegate(ui->subjectScheduleTableView));
    ui->subjectScheduleTableView->hideColumn(4); // Hide Local
    scheduleModel->select();

    // Detailed information about a holiday.
    EditableHolidayModel *model =
            new EditableHolidayModel(0, QSqlDatabase::database());
    ui->holidayEditTableView->setModel(model);

    monitor = QGeoPositionInfoSource::createDefaultSource(this);

    QObject::connect(monitor, SIGNAL(error(QGeoPositionInfoSource::Error)), this,
                     SLOT(errorRecv(QGeoPositionInfoSource::Error)));
    QObject::connect(monitor, SIGNAL(positionUpdated(QGeoPositionInfo)), this,
                     SLOT(positionUpdated(QGeoPositionInfo)));
    QObject::connect(monitor, SIGNAL(updateTimeout()), this, SLOT(updateTimeoutRecv()));

    ui->positionSourceLineEdit->setText(monitor->sourceName());

    // Initialize next day.
    nextClass.day = weekDay;
    nextClass.begin = nextClass.end = QTime::currentTime();
    updateNextClassAndSetTimer();

    QObject::connect(&nextCheck, SIGNAL(timeout()), this,
                     SLOT(timeoutWrapper()));

    // Set UI defaults.
    ui->subjectBeginningDateEdit->setDate(today);
    ui->subjectEndDateEdit->setDate(today);
    ui->holidayBeginningDateEdit->setDate(today);
    ui->holidayEndDateEdit->setDate(today);
    ui->presenceDateEdit->setDate(today);

    // Hide interfaces.
    ui->scheduleEditCheckBox->setChecked(false);
    ui->subjectTableEditCheckBox->setChecked(false);
    ui->presenceEditCheckBox->setChecked(false);
    ui->holidayEditCheckBox->setChecked(false);
    ui->subjectTableEditCheckBox->setChecked(false);

    this->blockSignals(false);
}

MainWindow::~MainWindow() { delete ui; }

void MainWindow::on_subjectEditorTab_tabBarClicked(int index) {
    switch (index) {
    default:
        break;
    case 0: // Update subject table view.
        static_cast<EditableSubjectPresenceModel *>(ui->subjectTableView->model())
                ->refresh();
        break;
    case 1: // Exhibit all information related to the subject.
        exhibitSubjectInfo();
        break;
    case 2: // Exhibit the schedule related to the selected subject.
        exhibitSubjectSchedule();
        break;
    case 3:
        exhibitSubjectPresence();
        break;
    }
}

void MainWindow::on_holidayPushButton_clicked() {
    QSqlQuery insertHoliday(QSqlDatabase::database());
    const QString name = ui->holidayNameLineEdit->text();

    // Check whether a holiday with the same name already exists
    insertHoliday.prepare("SELECT COUNT(*) FROM Holiday WHERE Name = :name");
    insertHoliday.bindValue(":name", name);
    insertHoliday.exec();
    qDebug() << insertHoliday.lastError();
    insertHoliday.next();

    if (insertHoliday.value(0).toInt() == 0) { // Insert holiday
        const QDate begin = ui->holidayBeginningDateEdit->date(),
                end = ui->holidayEndDateEdit->date();

        for (QDate it = begin; it <= end; it = it.addDays(1)) {
            insertHoliday.prepare(
                        "INSERT INTO Holiday (Name, Hdate) VALUES (:name, :date)");
            insertHoliday.bindValue(":name", name);
            insertHoliday.bindValue(":date", it);
            insertHoliday.exec();
            qDebug() << insertHoliday.lastError();
        }
        static_cast<EditableHolidayModel *>(ui->holidayEditTableView->model())
                ->refresh();
    }
}

void MainWindow::on_holidayDeleteButton_clicked() {
    EditableHolidayModel *model =
            static_cast<EditableHolidayModel *>(ui->holidayEditTableView->model());
    QString name = model->index(ui->holidayEditTableView->currentIndex().row(), 0)
            .data()
            .toString();
    QSqlQuery deleteHoliday(QSqlDatabase::database());

    deleteHoliday.prepare("DELETE FROM Holiday WHERE Name = :name");
    deleteHoliday.bindValue(":name", name);
    deleteHoliday.exec();

    qDebug() << "Name: " << name << " Error: " << deleteHoliday.lastError();
    model->refresh();
}

void MainWindow::on_holidayBeginningDateEdit_editingFinished() {
    const QDate &date = ui->holidayBeginningDateEdit->date();
    if (date > ui->holidayEndDateEdit->date())
        ui->holidayEndDateEdit->setDate(date);
}

void MainWindow::on_holidayEndDateEdit_editingFinished() {
    const QDate &date = ui->holidayEndDateEdit->date();
    if (date < ui->holidayBeginningDateEdit->date())
        ui->holidayBeginningDateEdit->setDate(date);
}

void MainWindow::on_scheduleBeginTime_editingFinished() {
    const QTime &time = ui->scheduleBeginTime->time();
    if (time > ui->scheduleEndTime->time())
        ui->scheduleEndTime->setTime(time);
}

void MainWindow::on_scheduleEndTime_editingFinished() {
    const QTime &time = ui->scheduleEndTime->time();
    if (time < ui->scheduleBeginTime->time())
        ui->scheduleBeginTime->setTime(time);
}

void MainWindow::on_scheduleWeekdayComboBox_currentIndexChanged(int index) {
    QSqlRelationalTableModel *model =
            static_cast<QSqlRelationalTableModel *>(ui->scheduleTableView->model());
    model->setFilter(QString("Day = ").append(QString::number(index)));
    model->select();
}

void MainWindow::on_subjectBeginningDateEdit_editingFinished() {
    const QDate &date = ui->subjectBeginningDateEdit->date();
    if (date > ui->subjectEndDateEdit->date())
        ui->subjectEndDateEdit->setDate(date);
}

void MainWindow::on_subjectEndDateEdit_editingFinished() {
    const QDate &date = ui->subjectEndDateEdit->date();
    if (date < ui->subjectBeginningDateEdit->date())
        ui->subjectBeginningDateEdit->setDate(date);
}

void MainWindow::on_subjectInsertPushButton_clicked() {
    QSqlQuery insertSubject(QSqlDatabase::database());

    insertSubject.prepare("INSERT INTO Subject (Name, Beginning, Ending, "
                          "MinimumFrequency) VALUES(:name, :begin, :end, "
                          ":minfreq)");
    insertSubject.bindValue(":name", ui->subjectNameLineEdit->text());
    insertSubject.bindValue(":begin", ui->subjectBeginningDateEdit->date());
    insertSubject.bindValue(":end", ui->subjectEndDateEdit->date());
    insertSubject.bindValue(":minfreq", ui->subjectMinFreqDoubleSpinBox->value());
    insertSubject.exec();

    qDebug() << insertSubject.lastError();

    // Refresh model.
    static_cast<EditableSubjectPresenceModel *>(ui->subjectTableView->model())
            ->refresh();
}

void MainWindow::on_subjectDeletePushButton_clicked() {
    QSqlQuery removeSubject(QSqlDatabase::database());

    removeSubject.prepare("DELETE FROM Subject WHERE ID = :id");
    removeSubject.bindValue(":id", currentSubjectID());
    bool ok = removeSubject.exec();
    qDebug() << removeSubject.lastError();

    if (!ok)
        return;

    // Refresh models.
    static_cast<EditableSubjectPresenceModel *>(ui->subjectTableView->model())
            ->refresh();
    static_cast<QSqlRelationalTableModel *>(ui->scheduleTableView->model())
            ->select();

    //TODO: Optimize based on the next class.
    updateNextClassAndSetTimer();
}

void MainWindow::on_scheduleInsertButton_clicked() {
    QSqlQuery insertSchedule(QSqlDatabase::database());
    int ret, day = ui->scheduleWeekdayComboBox->currentIndex();
    bool ok;

    // Check for schedule conflicts.
    QTime begin = ui->scheduleBeginTime->time(),
            end = ui->scheduleEndTime->time();

    insertSchedule.prepare("SELECT EXISTS (SELECT 1 FROM Schedule WHERE Day = "
                           ":day AND ("
                           "(Beginning <= :begina AND Ending > :beginb) "
                           "OR (Beginning < :enda AND Ending >= :endb) "
                           "OR (Beginning >= :beginc AND Beginning < :endc)"
                           "OR (Ending > :begind AND Ending <= :endd)"
                           ") LIMIT 1)");
    insertSchedule.bindValue(":day", day);
    insertSchedule.bindValue(":begina", begin);
    insertSchedule.bindValue(":beginb", begin);
    insertSchedule.bindValue(":beginc", begin);
    insertSchedule.bindValue(":begind", begin);
    insertSchedule.bindValue(":enda", end);
    insertSchedule.bindValue(":endb", end);
    insertSchedule.bindValue(":endc", end);
    insertSchedule.bindValue(":endd", end);
    insertSchedule.exec();
    if (!insertSchedule.next()){
        qDebug() << insertSchedule.lastError();
        return;
    }

    if (insertSchedule.value(0).toInt() == 1) {
        // TODO: Permitir seleção do usuário.
        qDebug() << "Conflito de horários";
        QSqlQuery removeFormerSchedule(QSqlDatabase::database());
        ret = QMessageBox::question(this, QObject::tr("Conflito de horários"),
                                   QObject::tr("Deseja substitituir ?"),
                                   QMessageBox::Yes | QMessageBox::No,
                                   QMessageBox::Yes);
        switch (ret) {
        case QMessageBox::Yes:
            removeFormerSchedule.prepare(
                        "DELETE FROM Schedule WHERE Day = :day "
                        "AND ("
                        "(Beginning <= :begina AND Ending > :beginb) "
                        "OR (Beginning < :enda AND Ending >= :endb) "
                        "OR (Beginning >= :beginc AND Beginning < :endc)"
                        "OR (Ending > :begind AND Ending <= :endd)"
                        ")");
            removeFormerSchedule.bindValue(
                        ":day", day);
            removeFormerSchedule.bindValue(":begina", begin);
            removeFormerSchedule.bindValue(":beginb", begin);
            removeFormerSchedule.bindValue(":beginc", begin);
            removeFormerSchedule.bindValue(":begind", begin);
            removeFormerSchedule.bindValue(":enda", end);
            removeFormerSchedule.bindValue(":endb", end);
            removeFormerSchedule.bindValue(":endc", end);
            removeFormerSchedule.bindValue(":endd", end);
            removeFormerSchedule.exec();
            static_cast<QSqlRelationalTableModel *>(ui->scheduleTableView->model())
                    ->select();
            break;
        case QMessageBox::No:
        default:
            return;
        }
    }

    // Acquire the id of the subject with the given name.
    insertSchedule.prepare("SELECT ID FROM Subject WHERE Name = :name");
    insertSchedule.bindValue(":name", ui->scheduleSubjectComboBox->currentText());
    insertSchedule.exec();
    if (!insertSchedule.next())
        return;

    QTime now = QTime::currentTime();
    int subject = insertSchedule.value(0).toInt();

    // Insert the new schedule row.
    insertSchedule.prepare("INSERT INTO Schedule (Day, Beginning, Ending, "
                           "Subject) VALUES(:day, :begin, :end, :subject)");
    insertSchedule.bindValue(":day", day);
    insertSchedule.bindValue(":begin", begin);
    insertSchedule.bindValue(":end", end);
    insertSchedule.bindValue(":subject", subject);

    ok = insertSchedule.exec();
    qDebug() << insertSchedule.lastError();
    if (!ok)
        return;

    static_cast<QSqlRelationalTableModel *>(ui->scheduleTableView->model())
            ->select();

    // Update the next class.
    if (nextClass.day == INVALID_WEEKDAY ||
            (nextClass.day > day && begin > now) ||
            (nextClass.day == day && nextClass.begin > begin)) {
        nextClass.day = day;
        nextClass.begin = begin;
        nextClass.end = end;
        nextClass.center = QGeoCoordinate();
        setTimer();
    }
}

void MainWindow::on_scheduleDeleteButton_clicked() {
    QSqlRelationalTableModel *model =
            static_cast<QSqlRelationalTableModel *>(ui->scheduleTableView->model());
    int row = ui->scheduleTableView->currentIndex().row(),
            day = model->index(row, 0).data().toInt();
    QTime begin = QTime::fromString(model->index(row, 1).data().toString()),
            now = QTime::currentTime();
    QSqlQuery removeSchedule(QSqlDatabase::database());

    removeSchedule.prepare(
                "DELETE FROM Schedule WHERE Day = :day AND Beginning = :begin");
    removeSchedule.bindValue(":day", day);
    removeSchedule.bindValue(":begin", begin);
    bool ok = removeSchedule.exec();

    qDebug() << removeSchedule.lastError();
    if (!ok)
        return;

    model->select();

    // Update the next class.
    if ((nextClass.day > day && begin > now) ||
            (nextClass.day == day && nextClass.begin >= begin))
        updateNextClassAndSetTimer();
}

void MainWindow::on_presenceInsertPushButton_clicked() {
    QSqlQuery insertPresence(QSqlDatabase::database());

    // Acquire the id of the subject with the given name.
    insertPresence.prepare("SELECT ID FROM Subject WHERE Name = :name");
    insertPresence.bindValue(":name", ui->presenceSubjectComboBox->currentText());
    insertPresence.exec();
    if (!insertPresence.next())
        return;

    int subject = insertPresence.value(0).toInt();

    insertPresence.prepare(
                "INSERT INTO Presence (Pdate, Subject) VALUES(:date, :subject)");
    insertPresence.bindValue(":date", ui->presenceDateEdit->date());
    insertPresence.bindValue(":subject", subject);
    insertPresence.exec();

    qDebug() << insertPresence.lastError();
    static_cast<QSqlRelationalTableModel *>(ui->presenceTableView->model())
            ->select();
}

void MainWindow::on_presenceDeletePushButton_clicked() {
    QSqlRelationalTableModel *model =
            static_cast<QSqlRelationalTableModel *>(ui->presenceTableView->model());
    QSqlQuery removePresence(QSqlDatabase::database());

    removePresence.prepare("DELETE FROM Presence WHERE ID = :id");
    removePresence.bindValue(
                ":id", model->index(ui->presenceTableView->currentIndex().row(), 0)
                .data()
                .toInt());
    removePresence.exec();

    qDebug() << removePresence.lastError();
    model->select();
}

void MainWindow::on_presenceCheckBox_clicked() { exhibitSubjectPresence(); }

void MainWindow::updateNextClassAndSetTimer() {
    bool ok = true;
    QSqlQuery qry(QSqlDatabase::database());

    // TODO: Arrumar queries.
    qry.prepare("SELECT Day, Beginning, Ending, Subject, Local FROM Schedule "
                "WHERE Day = :daya AND Ending > :end OR Day > :dayb "
                "ORDER BY Day, Beginning LIMIT 1"
                );
    qry.bindValue(":daya", nextClass.day);
    qry.bindValue(":dayb", nextClass.day);
    qry.bindValue(":end", nextClass.end);
    qry.exec();
    if (!qry.next())
    {
        qDebug() << qry.lastError();
        return;
    }

    nextClass.day = qry.value(0).toInt(&ok);
    if (ok) {
        nextClass.begin = QTime::fromString(qry.value(1).toString());
        nextClass.end = QTime::fromString(qry.value(2).toString());
        nextClass.subject = qry.value(3).toInt();
        QVariant rawArea = qry.value(4);

        if (!rawArea.isValid()) {
            nextClass.center = QGeoCoordinate();
        } else {
            QDataStream in(rawArea.toByteArray());
            in >> nextClass.center;
        }

        setTimer();
    }
    else {
        qDebug() << qry.lastError();
        nextClass.day = INVALID_WEEKDAY;
    }
}

void MainWindow::timeoutWrapper()
{
    monitor->requestUpdate();
}

void MainWindow::errorRecv(const QGeoPositionInfoSource::Error &err) {
    qDebug() << "Positioning error!\n";
    QString errMsg;
    switch (err) {
    case QGeoPositionInfoSource::AccessError:
        errMsg = "The connection setup to the remote positioning backend failed "
                 "because the application lacked the required privileges.";
        break;
    case QGeoPositionInfoSource::ClosedError:
        errMsg = "The remote positioning backend closed the connection, which "
                 "happens for example in case the user is switching location "
                 "services to off. As soon as the location service is re-enabled "
                 "regular updates will resume.";
        break;
    case QGeoPositionInfoSource::NoError:
        errMsg = "No error has occurred.";
        break;
    case QGeoPositionInfoSource::UnknownSourceError:
        errMsg = "An unidentified error occurred.";
        break;
    default:
        break;
    }
    ui->positionErrorLineEdit->setText(errMsg);
    qDebug() << errMsg;
    monitor->requestUpdate(30000);
}

void MainWindow::positionUpdated(const QGeoPositionInfo &update) {
    // Check if the user associated a position with the next class.
    if (!nextClass.center.isValid()) {
        QSqlQuery updateLocal(QSqlDatabase::database());
        QGeoCoordinate coord = update.coordinate();
        QByteArray rawCoord;
        QDataStream out(&rawCoord, QIODevice::WriteOnly);
        out << coord;
        int ret = QMessageBox::question(
                    this,
                    QObject::tr("Não há local associado ao horário atual."),
                    QObject::tr("Você deseja associar a posição atual à aula atual ?"),
                    QMessageBox::Yes | QMessageBox::No | QMessageBox::NoToAll,
                    QMessageBox::Yes);
        switch (ret) {
        case QMessageBox::Yes:
            updateLocal.prepare("UPDATE Schedule SET Local = :local WHERE Day = :day "
                                "AND Beginning = :begin");
            updateLocal.bindValue(":local", rawCoord);
            updateLocal.bindValue(":day", nextClass.day);
            updateLocal.bindValue(":begin", nextClass.begin);
            updateLocal.exec();
            nextClass.center = coord;
            break;
        case QMessageBox::No:
            if (QTime::currentTime().addMSecs(nextClass.recheck) < nextClass.end)
                nextCheck.start(nextClass.recheck);
            else
                updateNextClassAndSetTimer();
            return;
        case QMessageBox::NoToAll:
            updateNextClassAndSetTimer();
            return;
        default:
            return;
        }
    }

    // Check if the user is 20 meters away from the class.
    else if (update.coordinate().distanceTo(nextClass.center) > 20.00) {
        if (QTime::currentTime().addMSecs(nextClass.recheck) < nextClass.end)
            nextCheck.start(nextClass.recheck);
        else
            updateNextClassAndSetTimer();
        return;
    }

    QSqlQuery insertPresence(QSqlDatabase::database());
    insertPresence.prepare(
                "INSERT INTO Presence (Pdate, Subject) VALUES(:date, :subject)");
    insertPresence.bindValue(":date", QDate::currentDate());
    insertPresence.bindValue(":subject", nextClass.subject);
    insertPresence.exec();

    ui->positionCoordinateLineEdit->setText(update.coordinate().toString());
    ui->positionTimestampLineEdit->setText(update.timestamp().toString());

    qDebug() << "Received:\n" << ui->positionTimestampLineEdit->text() << "\n"
             << ui->positionCoordinateLineEdit->text();

    updateNextClassAndSetTimer();
}

void MainWindow::updateTimeoutRecv() {
    ui->positionTimeoutSpinBox->setValue(ui->positionTimeoutSpinBox->value() + 1);

    monitor->requestUpdate(30000);

    qDebug() << "Timeout\nLast position:\n"
             << ui->positionTimestampLineEdit->text() << "\n"
             << ui->positionCoordinateLineEdit->text() << "\nRetrying...";
}

void MainWindow::checkSubjectEmpty(const QSqlDatabase &db) {
    QSqlQuery subjectEmpty(db);

    subjectEmpty.exec("SELECT count(*) FROM sqlite_master WHERE type='table' AND "
                      "name='Subject'");
    subjectEmpty.next();
    subjectIsEmpty = subjectEmpty.value(0).toInt() == 0;
}

int MainWindow::currentSubjectID() {
    EditableSubjectPresenceModel *subjectModel =
            static_cast<EditableSubjectPresenceModel *>(
                ui->subjectTableView->model());
    return subjectModel->data(subjectModel->index(
                                  ui->subjectTableView->currentIndex().row(), 0))
            .toInt();
}

void MainWindow::exhibitSubjectInfo() {
    EditableSubjectPresenceModel *subjectModel =
            static_cast<EditableSubjectPresenceModel *>(
                ui->subjectTableView->model());
    int row = ui->subjectTableView->currentIndex().row(),
            id = subjectModel->data(subjectModel->index(row, 0)).toInt();

    if (id == 0)
        return;

    int attended = subjectModel->data(subjectModel->index(row, 5)).toInt(),
            currentClasses, totalClasses, futureClasses, misses;

    ui->detailedNameLineEdit->setText(
                subjectModel->data(subjectModel->index(row, 1)).toString());

    QDate begin = QDate::fromString(
                subjectModel->data(subjectModel->index(row, 2)).toString(),
                "d/M/yyyy"),
            end = QDate::fromString(
                subjectModel->data(subjectModel->index(row, 3)).toString(),
                "d/M/yyyy");

    ui->detailedBeginDateEdit->setDate(begin);
    ui->detailedEndDateEdit->setDate(end);
    ui->detailedFrequencyDoubleSpinBox->setValue(
                subjectModel->data(subjectModel->index(row, 4)).toDouble());
    ui->detailedPresenceSpinBox->setValue(attended);

    QSqlRelationalTableModel *subjectScheduleModel =
            static_cast<QSqlRelationalTableModel *>(
                ui->subjectScheduleTableView->model());
    QSqlDatabase db = subjectScheduleModel->database();

    // Find out which days of the week have a class of the subject.
    QBitArray days(7, false);
    QDate buffer, today = QDate::currentDate();
    int weekDay, todayWeekDay = Subject::getCompatibleWeekDay(today.dayOfWeek());
    QTime endTime(0, 0);
    QSqlQuery q(db);
    q.prepare("SELECT DISTINCT A.Day, A.Ending FROM Schedule A JOIN Subject B "
              "ON A.Subject = B.ID AND B.ID = :id");
    q.bindValue(":id", id);
    q.exec();
    qDebug() << q.lastError();

    while (q.next()) {
        weekDay = q.value(0).toInt();
        days.setBit(weekDay);
        if (weekDay == todayWeekDay)
            endTime = QTime::fromString(q.value(1).toString());
    }

    // Calculate total classes, past classes and coming classes.
    totalClasses = Subject::getClassesCount(begin, end, days, db);
    ui->totalClassesSpinBox->setValue(totalClasses);

    buffer = QTime::currentTime() < endTime ? today : today.addDays(1);
    futureClasses = Subject::getClassesCount(buffer, end, days, db);
    currentClasses = totalClasses - futureClasses;
    ui->remainingClassesSpinBox->setValue(futureClasses);

    // Calculate misses.
    misses = currentClasses - attended;
    ui->missesSpinBox->setValue(misses);

    // Calculate and exhibit current frequency.
    ui->currentFrequencyDoubleSpinBox->setValue(
                Subject::getCurrentFrequency(totalClasses, misses) * 100.0);

    // Calculate the remaining misses.
    ui->remainingMissesSpinBox->setValue(Subject::getRemainingMisses(
                                             misses,
                                             totalClasses,
                                             ui->detailedFrequencyDoubleSpinBox->value() / 100.0));
}

void MainWindow::exhibitSubjectSchedule() {
    QSqlRelationalTableModel *scheduleModel =
            static_cast<QSqlRelationalTableModel *>(
                ui->subjectScheduleTableView->model());
    scheduleModel->setFilter(
                QString("Subject = ").append(QString::number(currentSubjectID())));
    scheduleModel->select();
}

void MainWindow::exhibitSubjectPresence() {
    QSqlRelationalTableModel *model = static_cast<QSqlRelationalTableModel *>(ui->presenceTableView->model());
    if (ui->presenceCheckBox->isChecked())
        model->setFilter(
                QString("Subject = ").append(QString::number(currentSubjectID())));

    else
        model->setFilter(QString());
    model->select();
}

void MainWindow::setTimer() {
    QDateTime now = QDateTime::currentDateTime();
    int msec, daysTo, weekDay = Subject::getCompatibleWeekDay(now.date().dayOfWeek());

    daysTo = nextClass.day - weekDay;
    if (daysTo < 0)
        daysTo += 7;

    QDateTime next(now.addDays(daysTo).date(), nextClass.begin);
    nextClass.recheck = nextClass.begin.msecsTo(nextClass.end) / 5;
    msec = now.msecsTo(next);
    if (msec < 0)
        msec = 0;

    nextCheck.start(msec);
}
