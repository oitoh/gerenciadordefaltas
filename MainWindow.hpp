#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QGeoPositionInfoSource>
#include <QGeoCoordinate>
#include <QMainWindow>
#include <QSqlQuery>
#include <QTimer>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_subjectEditorTab_tabBarClicked(int index);

    void on_holidayPushButton_clicked();

    void on_holidayDeleteButton_clicked();

    void on_holidayBeginningDateEdit_editingFinished();

    void on_holidayEndDateEdit_editingFinished();

    void on_scheduleBeginTime_editingFinished();

    void on_scheduleEndTime_editingFinished();

    void on_scheduleWeekdayComboBox_currentIndexChanged(int index);

    void on_subjectBeginningDateEdit_editingFinished();

    void on_subjectEndDateEdit_editingFinished();

    void on_subjectInsertPushButton_clicked();

    void on_subjectDeletePushButton_clicked();

    void on_scheduleInsertButton_clicked();

    void on_scheduleDeleteButton_clicked();

    void on_presenceInsertPushButton_clicked();

    void on_presenceDeletePushButton_clicked();

    void on_presenceCheckBox_clicked();

    void errorRecv(const QGeoPositionInfoSource::Error &err);

    void positionUpdated(const QGeoPositionInfo &update);

    void updateTimeoutRecv();

    void updateNextClassAndSetTimer();

    void timeoutWrapper();

private:
    Ui::MainWindow *ui;

    QGeoPositionInfoSource *monitor;

    struct {
        int day,
        subject,
        recheck;
        QTime begin,
        end;
        QGeoCoordinate center;
    } nextClass;

    QTimer nextCheck;

    bool subjectIsEmpty;

    void checkSubjectEmpty(const QSqlDatabase &db);

    inline int currentSubjectID();

    void exhibitSubjectInfo();

    void exhibitSubjectSchedule();

    void exhibitSubjectPresence();

    void setTimer();

};

#endif // MAINWINDOW_HPP
