#include "ExtendedScheduleModel.hpp"

// TODO: Melhorar a atualização do horário, criando uma condição para atualizar o dia.

ExtendedScheduleModel::ExtendedScheduleModel(QObject * parent, QSqlDatabase db)
    : QSqlRelationalTableModel(parent, db)
{

    QSqlRelationalTableModel::setTable("Schedule");

    QSqlRelationalTableModel::setHeaderData(SCHDLC_DAY, Qt::Horizontal, QObject::tr("Dia"));
    QSqlRelationalTableModel::setHeaderData(SCHDLC_BEGINNING, Qt::Horizontal, QObject::tr("Inicio"));
    QSqlRelationalTableModel::setHeaderData(SCHDLC_ENDING, Qt::Horizontal, QObject::tr("Fim"));
    QSqlRelationalTableModel::setHeaderData(SCHDLC_SUBJECT, Qt::Horizontal, QObject::tr("Disciplina"));
    QSqlRelationalTableModel::setHeaderData(SCHDLC_LOCAL, Qt::Horizontal, QObject::tr("Local"));

    QSqlRelationalTableModel::setRelation(3, QSqlRelation("Subject", "ID", "Name"));
}

bool ExtendedScheduleModel::setData(const QModelIndex & index, const QVariant & value, int role)
{
    if(role == Qt::EditRole)
    {
        bool ok;
        int column = index.column(), day, result;
        QTime oldValue, newValue, otherTime;
        const QSqlRelationalTableModel *model = static_cast<const QSqlRelationalTableModel *>(index.model());
        QSqlQuery check;

        switch(column)
        {
            case SCHDLC_BEGINNING:
            case SCHDLC_ENDING:
                day = model->index(index.row(), SCHDLC_DAY).data().toInt(&ok);
                if (!ok)
                    return false;
                newValue = value.toTime();
                oldValue = index.data().toTime();
                switch(column)
                {
                    case SCHDLC_BEGINNING:
                        otherTime = model->index(index.row(), SCHDLC_ENDING).data().toTime();
                        check.prepare("SELECT EXISTS (SELECT 1 FROM Schedule WHERE Day = "
                                                   ":day AND Beginning <> :cbegin AND ("
                                                   "(Beginning <= :begina AND Ending > :beginb) "
                                                   "OR (Beginning < :enda AND Ending >= :endb) "
                                                   "OR (Beginning >= :beginc AND Beginning < :endc)"
                                                   "OR (Ending > :begind AND Ending <= :endd)"
                                                   ") LIMIT 1)");
                        check.bindValue(":day", day);
                        check.bindValue(":cbegin", oldValue);
                        check.bindValue(":begina", newValue);
                        check.bindValue(":beginb", newValue);
                        check.bindValue(":beginc", newValue);
                        check.bindValue(":begind", newValue);
                        check.bindValue(":enda", otherTime);
                        check.bindValue(":endb", otherTime);
                        check.bindValue(":endc", otherTime);
                        check.bindValue(":endd", otherTime);
                        check.exec();
                        if (!check.next())
                        {
                            qDebug() << check.lastError();
                            return false;
                        }
                        result = check.value(0).toInt(&ok);
                        if (!ok || result == 1)
                            return false;
                        else
                            break;
                    case SCHDLC_ENDING:
                        otherTime = model->index(index.row(), SCHDLC_BEGINNING).data().toTime();
                        check.prepare("SELECT EXISTS (SELECT 1 FROM Schedule WHERE Day = "
                                                   ":day AND Ending <> :cend AND ("
                                                   "(Beginning <= :begina AND Ending > :beginb) "
                                                   "OR (Beginning < :enda AND Ending >= :endb) "
                                                   "OR (Beginning >= :beginc AND Beginning < :endc)"
                                                   "OR (Ending > :begind AND Ending <= :endd)"
                                                   ") LIMIT 1)");
                        check.bindValue(":day", day);
                        check.bindValue(":cend", oldValue);
                        check.bindValue(":begina", otherTime);
                        check.bindValue(":beginb", otherTime);
                        check.bindValue(":beginc", otherTime);
                        check.bindValue(":begind", otherTime);
                        check.bindValue(":enda", newValue);
                        check.bindValue(":endb", newValue);
                        check.bindValue(":endc", newValue);
                        check.bindValue(":endd", newValue);
                        check.exec();
                        if (!check.next())
                        {
                            qDebug() << check.lastError();
                            return false;
                        }
                        result = check.value(0).toInt(&ok);
                        if (!ok || result == 1)
                            return false;
                        else
                            break;
                }
            default:
                break;
        }
    }

    return QSqlRelationalTableModel::setData(index, value, role);
}
