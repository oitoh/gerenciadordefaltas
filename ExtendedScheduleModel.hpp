#ifndef EXTENDEDSCHEDULEMODEL_HPP
#define EXTENDEDSCHEDULEMODEL_HPP

#include <QtSql>

enum ScheduleColumns{
    SCHDLC_DAY,
    SCHDLC_BEGINNING,
    SCHDLC_ENDING,
    SCHDLC_SUBJECT,
    SCHDLC_LOCAL,
    SCHDLC_NUM
};

class ExtendedScheduleModel : public QSqlRelationalTableModel
{
public:
    ExtendedScheduleModel(QObject *parent = 0, QSqlDatabase db = QSqlDatabase());

    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);
};

#endif // EXTENDEDSCHEDULEMODEL_HPP
