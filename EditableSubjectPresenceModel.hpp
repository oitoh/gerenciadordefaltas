#ifndef EDITABLESUBJECTPRESENCEMODEL_HPP
#define EDITABLESUBJECTPRESENCEMODEL_HPP
#include <QtSql>


class EditableSubjectPresenceModel : public QSqlQueryModel
{
public:
    EditableSubjectPresenceModel(QObject *parent, QSqlDatabase db);

    Qt::ItemFlags flags(const QModelIndex &index) const Q_DECL_OVERRIDE;

    bool setData(const QModelIndex &index, const QVariant &value, int) Q_DECL_OVERRIDE;

    void refresh();

private:
    QSqlDatabase database;

    bool setSubjectName(int id, const QString &name);

    bool setSubjectBeginning(int id, const QDate &begin, const QDate& end);

    bool setSubjectEnd(int id, const QDate &end, const QDate &begin);

    bool setSubjectMinFreq(int id, double minFreq);

};

#endif // EDITABLESUBJECTPRESENCEMODEL_HPP
