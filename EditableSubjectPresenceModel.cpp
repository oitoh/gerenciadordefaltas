#include "EditableSubjectPresenceModel.hpp"

EditableSubjectPresenceModel::EditableSubjectPresenceModel(QObject *parent, QSqlDatabase db)
    : QSqlQueryModel(parent), database(db)
{
    refresh();
}

Qt::ItemFlags EditableSubjectPresenceModel::flags(const QModelIndex &index) const
{
    Qt::ItemFlags flags = QSqlQueryModel::flags(index);
    if (index.column() >= 1 && index.column() <= 4)
        flags |= Qt::ItemIsEditable;
    return flags;
}

bool EditableSubjectPresenceModel::setData(const QModelIndex &index, const QVariant &value, int)
{
    if (index.column() < 1 || index.column() > 4)
        return false;

    QDate other;
    int row = index.row();
    QModelIndex primaryKeyIndex = QSqlQueryModel::index(row, 0);
    int id = data(primaryKeyIndex).toInt();

    bool ok;
    switch(index.column()) {
    case 1:
        ok = setSubjectName(id, value.toString());
        break;
    case 2:
        other = QDate::fromString(data(QSqlQueryModel::index(row, 3)).toString(), "d/M/yyyy");
        ok = setSubjectBeginning(id, QDate::fromString(value.toString(), "d/M/yyyy"), other);
        break;
    case 3:
        other = QDate::fromString(data(QSqlQueryModel::index(row, 2)).toString(), "d/M/yyyy");
        ok = setSubjectEnd(id, QDate::fromString(value.toString(), "d/M/yyyy"), other);
        break;
    case 4:
        ok = setSubjectMinFreq(id, value.toDouble());
        break;
    default:
        ok = false;
        break;
    }
    refresh();
    return ok;
}

void EditableSubjectPresenceModel::refresh()
{
    clear();
    setQuery("SELECT S.ID, S.Name, strftime('%d/%m/%Y', S.Beginning) AS Beginning, strftime('%d/%m/%Y', S.Ending) AS Ending, S.MinimumFrequency, COUNT(P.ID) AS Attended FROM Subject S LEFT OUTER JOIN Presence P ON S.ID = P.Subject GROUP BY S.ID ORDER BY S.Name", database);

    this->setHeaderData(0, Qt::Horizontal, "ID");
    this->setHeaderData(1, Qt::Horizontal, QObject::tr("Nome"));
    this->setHeaderData(2, Qt::Horizontal,
                                      QObject::tr("Início"));
    this->setHeaderData(3, Qt::Horizontal, QObject::tr("Fim"));
    this->setHeaderData(4, Qt::Horizontal,
                                      QObject::tr("Frequência Mínima"));
    this->setHeaderData(5, Qt::Horizontal, QObject::tr("Presenças"));
}

bool EditableSubjectPresenceModel::setSubjectName(int id, const QString &name)
{
    QSqlQuery updateName(database);
    updateName.prepare("UPDATE Subject SET Name = :name WHERE ID = :id");

    updateName.bindValue(":name", name);
    updateName.bindValue(":id", id);
    bool ok = updateName.exec();
    qDebug() << updateName.lastError();

    return ok;
}

bool EditableSubjectPresenceModel::setSubjectBeginning(int id, const QDate &begin, const QDate& end)
{
    bool ok = true;
    QSqlQuery updateBeginning(database);
    if ( begin > end )
    {
        updateBeginning.prepare("UPDATE Subject SET Ending = :end WHERE ID = :id");
        updateBeginning.bindValue(":end", begin);
        updateBeginning.bindValue(":id", id);
        ok = updateBeginning.exec();
        qDebug() << updateBeginning.lastError();
    }

    updateBeginning.prepare("UPDATE Subject SET Beginning = :begin WHERE ID = :id");
    updateBeginning.bindValue(":begin", begin);
    updateBeginning.bindValue(":id", id);
    ok = ok && updateBeginning.exec();
    qDebug() << updateBeginning.lastError();

    return ok;
}

bool EditableSubjectPresenceModel::setSubjectEnd(int id, const QDate &end, const QDate &begin)
{
    bool ok = true;
    QSqlQuery updateEnding(database);
    if ( end < begin )
    {
        updateEnding.prepare("UPDATE Subject SET Beginning = :begin WHERE ID = :id");
        updateEnding.bindValue(":begin", end);
        updateEnding.bindValue(":id", id);
        ok = updateEnding.exec();
        qDebug() << updateEnding.lastError();
    }

    updateEnding.prepare("UPDATE Subject SET Ending = :end WHERE ID = :id");
    updateEnding.bindValue(":end", end);
    updateEnding.bindValue(":id", id);
    ok = ok && updateEnding.exec();
    qDebug() << updateEnding.lastError();

    return ok;
}

bool EditableSubjectPresenceModel::setSubjectMinFreq(int id, double minFreq)
{
    QSqlQuery updateMinFreq(database);
    updateMinFreq.prepare("UPDATE Subject SET MinimumFrequency = :minFreq WHERE ID = :id");

    updateMinFreq.bindValue(":minFreq", minFreq);
    updateMinFreq.bindValue(":id", id);
    bool ok = updateMinFreq.exec();
    qDebug() << updateMinFreq.lastError();

    return ok;
}
