#-------------------------------------------------
#
# Project created by QtCreator 2015-12-16T21:16:35
#
#-------------------------------------------------

QT     += core gui sql positioning
CONFIG += c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Faltometro
TEMPLATE = app


SOURCES += main.cpp\
        MainWindow.cpp \
    EditableSubjectPresenceModel.cpp \
    subjectCalculations.cpp \
    EditableHolidayModel.cpp \
    ExtendedScheduleModel.cpp

HEADERS  += MainWindow.hpp \
    EditableSubjectPresenceModel.hpp \
    subjectCalculations.hpp \
    EditableHolidayModel.hpp \
    ExtendedScheduleModel.hpp

FORMS    += MainWindow.ui

DISTFILES += \
    android/AndroidManifest.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradlew \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew.bat

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android
