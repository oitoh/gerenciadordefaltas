<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pt" sourcelanguage="en">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="MainWindow.ui" line="147"/>
        <location filename="MainWindow.ui" line="1466"/>
        <source>Schedule</source>
        <translation>Horário</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="183"/>
        <source>Sunday</source>
        <translation>Domingo</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="188"/>
        <source>Monday</source>
        <translation>Segunda</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="193"/>
        <source>Tuesday</source>
        <translation>Terça</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="198"/>
        <source>Wednesday</source>
        <translation>Quarta</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="203"/>
        <source>Thursday</source>
        <translation>Quinta</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="208"/>
        <source>Friday</source>
        <translation>Sexta</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="213"/>
        <source>Saturday</source>
        <translation>Sábado</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="409"/>
        <location filename="MainWindow.ui" line="587"/>
        <location filename="MainWindow.ui" line="659"/>
        <location filename="MainWindow.ui" line="1573"/>
        <location filename="MainWindow.ui" line="1716"/>
        <source>Edit</source>
        <translation>Editar</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="272"/>
        <location filename="MainWindow.ui" line="721"/>
        <location filename="MainWindow.ui" line="1259"/>
        <location filename="MainWindow.ui" line="1872"/>
        <source>End</source>
        <translation>Fim</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="432"/>
        <location filename="MainWindow.ui" line="687"/>
        <location filename="MainWindow.ui" line="1608"/>
        <location filename="MainWindow.ui" line="1766"/>
        <source>Insert</source>
        <translation>Inserir</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="447"/>
        <location filename="MainWindow.ui" line="700"/>
        <location filename="MainWindow.ui" line="1592"/>
        <location filename="MainWindow.ui" line="1793"/>
        <source>Delete</source>
        <translation>Remover</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="343"/>
        <location filename="MainWindow.ui" line="776"/>
        <location filename="MainWindow.ui" line="953"/>
        <location filename="MainWindow.ui" line="1812"/>
        <source>Beginning</source>
        <translation>Início</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="488"/>
        <location filename="MainWindow.ui" line="551"/>
        <location filename="MainWindow.ui" line="1557"/>
        <source>Subject</source>
        <translation>Disciplina</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="507"/>
        <source>Calendar</source>
        <translation>Calendário</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="625"/>
        <location filename="MainWindow.ui" line="899"/>
        <location filename="MainWindow.ui" line="1734"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="746"/>
        <location filename="MainWindow.ui" line="804"/>
        <source>dd/MM/yy</source>
        <translation></translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="871"/>
        <source>Detailed View</source>
        <translation>Detalhes</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="932"/>
        <source>Subject name</source>
        <translation>Nome da disciplina</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="1005"/>
        <location filename="MainWindow.ui" line="1299"/>
        <location filename="MainWindow.ui" line="1843"/>
        <location filename="MainWindow.ui" line="1896"/>
        <source>d/M/yyyy</source>
        <translation></translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="1036"/>
        <source>Minimum Frequency</source>
        <translation>Frequência mínima</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="1330"/>
        <source>Current frequency</source>
        <translation>Frequência atual</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="1096"/>
        <source>Misses</source>
        <translation>Faltas</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="1381"/>
        <source>Remaining misses</source>
        <translation>Faltas restantes</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="1144"/>
        <location filename="MainWindow.ui" line="1498"/>
        <source>Presence</source>
        <translation>Presença</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="1429"/>
        <source>Remaining classes</source>
        <translation>Aulas restantes</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="1202"/>
        <source>Total classes</source>
        <translation>Total de aulas</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="31"/>
        <source>Faltometro</source>
        <translation></translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="834"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;Min frequency&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="1507"/>
        <source>Selected subject only</source>
        <translation>Exibir apenas a disciplina selecionada</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="1655"/>
        <source>Holiday</source>
        <translation>Feriado</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="1930"/>
        <source>Position</source>
        <translation>Posição</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="1947"/>
        <source>Timestamp</source>
        <translation>Horário</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="1977"/>
        <source>Coordinate</source>
        <translation>Coordenada</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="2007"/>
        <source>Error</source>
        <translation>Erro</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="2037"/>
        <source>Source</source>
        <translation>Origem</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="2067"/>
        <source>Timeout</source>
        <translation>Tempo esgotado</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="ExtendedScheduleModel.cpp" line="11"/>
        <source>Day</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ExtendedScheduleModel.cpp" line="12"/>
        <location filename="MainWindow.cpp" line="60"/>
        <source>Beginning</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ExtendedScheduleModel.cpp" line="13"/>
        <location filename="MainWindow.cpp" line="61"/>
        <source>End</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ExtendedScheduleModel.cpp" line="14"/>
        <location filename="MainWindow.cpp" line="79"/>
        <source>Subject</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ExtendedScheduleModel.cpp" line="15"/>
        <source>Local</source>
        <translation></translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="57"/>
        <location filename="MainWindow.cpp" line="77"/>
        <source>ID</source>
        <translation></translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="58"/>
        <location filename="MainWindow.cpp" line="98"/>
        <source>Name</source>
        <translation></translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="63"/>
        <source>Minimum Frequency</source>
        <translation></translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="64"/>
        <source>Attended</source>
        <translation></translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="78"/>
        <source>Date</source>
        <translation></translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="99"/>
        <source>First day</source>
        <translation>Primeiro dia</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="100"/>
        <source>Last day</source>
        <translation>Segundo dia</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="321"/>
        <source>Schedule conflict</source>
        <translation>Conflito de horários</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="322"/>
        <source>Do you wish to overwrite ?</source>
        <translation>Você deseja substituir ?</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="544"/>
        <source>There&apos;s no position associated to the current class</source>
        <translation>Não há posição associada à aula atual</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="545"/>
        <source>Do you wish to associate your current position to the current class ?</source>
        <translation>Você deseja associar a sua posição atual à aula atual ?</translation>
    </message>
    <message>
        <location filename="main.cpp" line="77"/>
        <source>Sunday</source>
        <translation>Domingo</translation>
    </message>
    <message>
        <location filename="main.cpp" line="78"/>
        <source>Monday</source>
        <translation>Segunda</translation>
    </message>
    <message>
        <location filename="main.cpp" line="79"/>
        <source>Tuesday</source>
        <translation>Terça</translation>
    </message>
    <message>
        <location filename="main.cpp" line="80"/>
        <source>Wednesday</source>
        <translation>Quarta</translation>
    </message>
    <message>
        <location filename="main.cpp" line="81"/>
        <source>Thursday</source>
        <translation>Quinta</translation>
    </message>
    <message>
        <location filename="main.cpp" line="82"/>
        <source>Friday</source>
        <translation>Sexta</translation>
    </message>
    <message>
        <location filename="main.cpp" line="83"/>
        <source>Saturday</source>
        <translation>Sábado</translation>
    </message>
    <message>
        <location filename="main.cpp" line="87"/>
        <source>Corrupted database</source>
        <translation>Base de dados corrompida</translation>
    </message>
</context>
</TS>
