#include "subjectCalculations.hpp"

#include <QSqlQuery>
#include <QVariant>

int Subject::getCompatibleWeekDay(int weekDay) {
    return weekDay == 7 ? 0 : weekDay;
}

int Subject::getCurrentWeekDay() {
    return getCompatibleWeekDay(QDate::currentDate().dayOfWeek());
}

int Subject::getClassesCount(const QDate& beginning, const QDate& end, const QBitArray& days, const QSqlDatabase &db)
{ //<! Returns the numbers of classes.
    int i, fwdCount, lwdCount,
            weekDay = getCompatibleWeekDay(beginning.dayOfWeek()),
            dd = beginning.daysTo(end) + 1; //Calculates how many days there are from the beginning to the end date.

    if(dd <= 1)
        return dd == 1 && days.testBit(weekDay) ? 1 : 0;

    // Count the number of lessons in the first week.
    for(i = weekDay, fwdCount = 0; i < 7; ++i)
    {
        if(days.testBit(i))
            ++fwdCount;
    }

    dd -= 7 - weekDay;

    if (dd <= 0)
        return fwdCount - getHolidaysCount(beginning, end, days, db);

    // Count the number of lessons in the last week.
    i = getCompatibleWeekDay(end.dayOfWeek());

    dd -= i + 1;

    for(lwdCount = 0; i > -1; --i)
    {
        if(days.testBit(i))
            ++lwdCount;
    }

    if(dd <= 0)
        return fwdCount + lwdCount - getHolidaysCount(beginning, end, days, db);

    return (dd / 7) * days.count(true) //How many lessons between the first and the last week.
            + fwdCount //How many lessons on the first week.
            + lwdCount //How many lessons on the last week.
            - getHolidaysCount(beginning, end, days, db); //How many holidays there were.
}

double Subject::getCurrentFrequency(int classesCount, int misses)
{ //<! Provides the current frequency, not as a percentage.
    return ((double)classesCount - (double)misses) / (double)classesCount;
}

int Subject::getHolidaysCount(const QDate& beginning, const QDate& end, const QBitArray& days, const QSqlDatabase &db) {
    if (days.count(true) == 0)
        return 0;

    QSqlQuery query(db);
    QString queryStr("SELECT COUNT(*) AS Holidays FROM (SELECT Hdate, CAST(strftime('%w', Hdate) AS INTEGER) AS Weekday FROM Holiday WHERE Hdate BETWEEN :begindate AND :enddate) WHERE Weekday IN (");

    for (int i = 0; i < 7; ++i)
        if ( days.testBit(i) )
            queryStr.append(QString::number(i)).append(",");

    // Overwrite the last char with ')'
    queryStr.data()[queryStr.size() - 1] = QChar(')');

    query.prepare(queryStr);
    query.bindValue(":begindate", beginning);
    query.bindValue(":enddate", end);
    query.exec();

    int result = query.next() ? query.value(0).toInt() : 0;

    return result;
}

int Subject::getRemainingMisses(int misses, int totalClasses, double minFrequency)
{ //<! Provides the number of classes that can still be missed.
    int missable = (int)( totalClasses * (1.0 - minFrequency) ) - misses;
    return (0 < missable ? missable : 0);
}
